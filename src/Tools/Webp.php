<?

namespace Dev\Tools;

class Webp
{
	const ORIGINAL_IMAGE_ROOT = "upload/";
	const WEBP_IMAGE_ROOT = "upload/resize_webp/";

	static protected function createPathToWebp(string $originalFilePath): string
	{
		$path = \str_replace(static::ORIGINAL_IMAGE_ROOT, static::WEBP_IMAGE_ROOT, $originalFilePath);
		$dotPos = strripos($path, ".");
		$path = substr($path, 0, $dotPos + 1);
		$path .= "webp";
		return $path;
	}

	/**
	 * Получение пути до webp-копии изображения
	 *
	 * @param string $originalFilePath Путь до оригинального изображения
	 *
	 * @return string Путь до webp-копии, строка пустая если копии нет.
	 */
	static public function getPathToWebp(string $originalFilePath): string
	{
		$webpSrc = static::createPathToWebp($originalFilePath);

		if (!\file_exists($_SERVER["DOCUMENT_ROOT"] . $webpSrc)) {
			$webpSrc = "";
		}

		return $webpSrc;
	}

	/**
	 * Функция для ресайза изображения и получения путь до webp-копии, если такая есть
	 *
	 * @param int $id id изображения
	 * @param array $arParams - Параметры ресайза. Доступны ключи width, height - размеры создаваемой картинки, type - тип ресайза
	 *
	 * @return array Массив с ключамиDEFAULT_SRC - путь до уменьшенной картинки, WEBP_SRC - путь до webp копии, WIDTH,HEIGHT,SIZE - ширина, высота, размер
	 */
	static public function ResizeImageGet(int $id,  array $arParams = []): array
	{
		$arImageResult = [
			"DEFAULT_SRC" => "",
			"WEBP_SRC" => "",
			"WIDTH" => 0,
			"HEIGHT" => 0,
			"SIZE" => 0,
		];

		if (!class_exists('\CFile')) {
			return $arImageResult;
		}

		if (intval($id) == 0) {
			return $arImageResult;
		}

		foreach (["width", "height"] as $strItem) {
			if (intval($arParams[$strItem]) == 0) {
				$arParams[$strItem] = 200;
			}
		}

		if (empty($arParams["type"])) {
			$arParams["type"] = BX_RESIZE_IMAGE_EXACT;
		}

		list(
			"src" => $arImageResult["DEFAULT_SRC"],
			"width" => $arImageResult["WIDTH"],
			"height" => $arImageResult["HEIGHT"],
			"size" =>  $arImageResult["SIZE"],
		) = \CFile::ResizeImageGet(
			$id,
			[
				"width" => $arParams["width"],
				"height" => $arParams["height"]
			],
			$arParams["type"],
			true
		);
		if (empty($arImageResult["DEFAULT_SRC"])) {
			return $arImageResult;
		}

		$arImageResult["WEBP_SRC"] = static::getPathToWebp($arImageResult["DEFAULT_SRC"]);

		return $arImageResult;
	}
}
