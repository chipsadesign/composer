<?


namespace Dev;

use Bitrix\Main\Loader;

class Handlers
{
	/**
	 * cURL запрос
	 */
	public static function sendPostRequest($url, $data)
	{
		$curl = curl_init();
		$data = http_build_query($data);
		curl_setopt($curl, CURLOPT_URL, $url);
		curl_setopt($curl, CURLOPT_BINARYTRANSFER, 1);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);
		curl_setopt($curl, CURLOPT_HEADER, 0);
		curl_setopt($curl, CURLOPT_POST, 1);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
		$return_data = curl_exec($curl);
		curl_close($curl);

		return $return_data;
	}

	/**
	 * Функция для склонения слов
	 *
	 * @param  [type] $number [число]
	 * @param  [type] $arr    [массив слов]
	 * @return [type]         [слово]
	 *
	 *  sgkHandlers::getInclinationByNumber($arResult["COUNT"], array("товар", "товара", "товаров")
	 */
	public static function getInclinationByNumber(string $number, array $arr = []): string
	{
		$number = (string) $number;
		$numberEnd = substr($number, -2);
		$numberEnd2 = 0;
		if (strlen($numberEnd) == 2) {
			$numberEnd2 = $numberEnd[0];
			$numberEnd = $numberEnd[1];
		}
		if ($numberEnd2 == 1) return $arr[2];
		else if ($numberEnd == 1) return $arr[0];
		else if ($numberEnd > 1 && $numberEnd < 5) return $arr[1];
		else return $arr[2];
	}

	/**
	 * Функция проверяет является ли ссылка внешней
	 *
	 * @param mixed $strLink - ссылка
	 *
	 * @return bool
	 */
	static public function isExternalLink(string $strLink): bool
	{
		$boolResult = false;
		if (strpos($strLink, "http") === 0) {
			$strDomain = (\Bitrix\Main\Context::getCurrent()->getRequest()->isHttps() ? "https://" : "http://") . $_SERVER["SERVER_NAME"];
			if (strpos($strLink, $strDomain) === 0) {
				$boolResult = false;
			} else {
				$boolResult = true;
			}
		} else {
			$boolResult = false;
		}
		return $boolResult;
	}

	/**
	 * Подключение всех php файлов в директории
	 * 
	 * @param string $strPath - Путь к директории
	 * 
	 * @return [type]
	 */
	static public function includeAllFiles(string $strPath): void
	{
		$arFiles = glob($strPath . "/*.php");
		if (is_array($arFiles)) {
			foreach ($arFiles as $strFile) {
				include_once($strFile);
			}
		}
	}

	public static function clearPhone(string $phone)
	{
		$clearPhone = str_replace(["+", "(", ")", " ", "-"], "", $phone);
		return $clearPhone;
	}

	public static function getIblockIdByCode(string $code, string $contentType = ""): int
	{
		$id = 0;
		if ($code !== "") {
			Loader::includeModule("iblock");
			$arFilter = [
				"CODE" => $code,
			];
			if ($contentType !== "") {
				$arFilter["IBLOCK_TYPE_ID"] = $contentType;
			}
			$obIblock = \Bitrix\Iblock\IblockTable::getList([
				"filter" => $arFilter,
				"select" => [
					"ID",
				],
				"cache" => [
					"ttl" => 86400,
				]
			])->FetchObject();

			if (!\is_null($obIblock)) {
				$id = $obIblock->getID();
			}
		}

		return $id;
	}
}
