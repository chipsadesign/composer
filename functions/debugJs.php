<?

if (!function_exists("preJs")) {
	function preJs()
	{
		global $USER;

		if (
			$USER->IsAdmin()
			|| $_COOKIE["BITRIX_SM_DEBUG"] == "Y"
		) {
			$options = func_get_args();
			$val = array_shift($options);
?>
			<script data-skip-moving=true>
				console.log(<?= json_encode($val); ?>);
			</script>
<?
		}
	}
}
